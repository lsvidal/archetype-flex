package service;

import business.HelloSayer;

public class HelloService {
	public String sayHello() {
		HelloSayer hello = new HelloSayer();
		return hello.say();
	}
}